const express = require('express');
const fs = require('fs')
// assignment
const app = express();
const port = 3000;
const path = require('path');
const semuadata = require('./semuadata.js');

// 
let varsatu =  JSON.stringify(semuadata)
let vardua = JSON.parse(varsatu)
function bebas(a, b) {
    let identitas = []
    identitas = a.filter((key) => {
        return key.age == b.age || key.eyeColor == b.eyeColor || key.company == b.company ? key: '' 
    })
    return identitas 
}

// menggunakan ejs
app.set('view engine', 'ejs');
app.set('views', path.join( __dirname, 'views'));

// memasang express static
app.set('views', path.join( __dirname, 'public'));
app.use('/public', express.static(path.join(__dirname, 'public')));

// menghubungkan ke index.ejs
app.get('/', (req, res) => {
    res.render('index');
});

// menghubungkan ke halaman data
app.get('/data5', (req, res) => {
    let datadata = bebas(vardua, req.query)
    console.log(req.query)
    res.render(
        'about', {datadata}
    );
});

// menghubungkan ke halaman about
app.get('/about', (req, res) => {
    let datadata = semuadata
    res.render('about', {datadata});

});

// menghubungkan ke halaman 404
app.get('/404', (req, res) => {
    res.render('404');
});

// menghubungkan ke halaman 404 jika url tidak ada
app.use('/', (req, res) => {
    res.render('404');
    res.status(404);
});

app.listen(port, () => {
    console.log(`Example app listerning at http://localhost:${port}`)
});
